#second part task 1

import json
import time
import requests

def http_request(url):
    headers = {
        'Accept': 'application/json'
    }

    max_retries = 3
    retries = 0

    while retries < max_retries:
        try:
            response = requests.get(url, headers=headers)
            if response.status_code == 200:
                try:
                    json_response = response.json()
                    return json_response  # Return the JSON response
                except json.JSONDecodeError:
                    print("Response is not in valid JSON format.")
            else:
                print("Unexpected status code:", response.status_code)
        except requests.exceptions.RequestException as e:
            print("Error:", e)

        # Increment retries and wait before retrying
        retries += 1
        if retries < max_retries:
            print("Retrying...")
            time.sleep(1)  # Wait for 1 second before retrying

    return None  # Return None if maximum retries exceeded without success

# URL to send the GET request
url = 'https://eoa0qzkprh8wd6s.m.pipedream.net'

# Call the function with the URL
json_response = http_request(url)
print(json_response)

# task 2

import requests

headers = {
    'Accept': 'application/json',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36',
}

url = 'https://eo6f3hp6twlkn24.m.pipedream.net/'

for i in range(10):
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        break


#task 3
from datetime import datetime, timedelta
import calendar

def date_decorator(func): #decorator takes another function 
    def wrap(*args, **kwargs):# wrap function takes any no of positional arugemnet 
        date = func(*args, **kwargs)  #takes arugement and store it in date 
        return date.strftime('%Y-%m-%d')  #convert date into the given format
    return wrap 

@date_decorator
def first_day_of_last_week():  
    now = datetime.now()   #gives the current date
    _, last_day = calendar.monthrange(now.year, now.month)   #gives  current month and year

    last_date = datetime(now.year, now.month, last_day) # gives current year current month and last day
    start_of_last_week = last_date - timedelta(days=last_date.weekday())   #calculates the first date of week if last_date is a Sunday, this will give the date of the previous Monday, which is the start of the last week.
    return start_of_last_week

print(first_day_of_last_week())


# task 4
import unittest

class TestCacheDecorator(unittest.TestCase):
    def setUp(self):
        self.decorator = CacheDecorator()

    def test_cache_decorator_with_single_argument_function(self):
        @self.decorator
        def add_one(x):
            return x + 1

        self.assertEqual(add_one(1), 2)
        self.assertEqual(add_one(2), 3)
        self.assertEqual(add_one(1), 2)  # This should return 2 from cache

    def test_cache_decorator_with_multiple_argument_function(self):
        @self.decorator
        def add(x, y):
            return x + y

        self.assertEqual(add(1, 2), 3)
        self.assertEqual(add(2, 3), 5)
        self.assertEqual(add(1, 2), 3)  # This should return 3 from cache

    def test_cache_decorator_with_no_argument_function(self):
        @self.decorator
        def return_one():
            return 1

        self.assertEqual(return_one(), 1)
        self.assertEqual(return_one(), 1)  # This should return 1 from cache

if __name__ == '__main__':
    unittest.main()

# task 5

# Define a metaclass that will modify the behavior of the classes it creates
class LoginMetaClass(type):
    # The initializer method for the metaclass
    def __init__(cls, name, bases, attrs):
        # Call the initializer of the base class (type) to create the class
        super().__init__(name, bases, attrs)
        # Save the original access_website method of the class
        original_access_website = cls.access_website

        # Define a new method that checks if the user is logged in before accessing the website
        def access_website(self, *args, **kwargs):
            # If the user is not logged in, raise an exception
            if not self.logged_in:
                raise Exception("You must be logged in to access the website.")
            # If the user is logged in, call the original access_website method
            return original_access_website(self, *args, **kwargs)

        # Replace the original access_website method in the class with the new method
        cls.access_website = access_website


# Define a class for accessing the website, using LoginMetaClass as its metaclass
class AccessWebsite(metaclass=LoginMetaClass):
    # Initialize the logged_in attribute to False
    logged_in = False

    # Define a method for logging in
    def login(self, username, password):
        # If the username and password are both "admin", set logged_in to True
        if username == "admin" and password == "admin":
            self.logged_in = True

    # Define the original access_website method that returns "Success"
    def access_website(self):
        return "Success"

