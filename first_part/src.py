#first part  
#Task 1

def recurrent_sequence(keywords):
    max_sequence, max_frequency = None, 0  # Initialize max_sequence and max_frequency

    for keyword in keywords:  # Iterate over each keyword in the list
        for i in range(len(keyword) - 1):  # Iterate over each index in the keyword
            sequence = keyword[i:i+2]  # Extract a sequence of two characters
            frequency = keyword.count(sequence)  # Count the frequency of the sequence in the keyword

            # If the current sequence's frequency is higher, or it's equal but the sequence is smaller
            if frequency > max_frequency or (frequency == max_frequency and sequence < max_sequence):
                max_sequence, max_frequency = sequence, frequency  # Update max_sequence and max_frequency

    return max_sequence  # Return the most recurrent sequence

keywords = input("Enter the list of keywords separated by spaces: ").split()  # Get the list of keywords from the user
result =recurrent_sequence(keywords)  # Find the most recurrent sequence
print("Most recurrent sequence:", result)  # Print the result

#task 2

import re

def extract_nutrition_values(text):
    nutrition_dict = {}

    # Extract nutritional values using pattern 
    matches = re.findall(r'([A-Za-zÀ-ÖØ-öø-ÿ\s]+)\s*:\s*([\d.,]+)\s*([A-Za-z%µ]+)', text)

    # Build dictionary in given format
    for match in matches:
        nutrition_dict[match[0]] = {'value': match[1], 'unit': match[2]}

    return nutrition_dict

#  implement it for given Example 
text = """
Additifs nutritionnels : Vitamine D3 : 160 UI, Fer (3b103) : 4mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg,Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603,3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %.
Additifs nutritionnels : Vitamine C-D3 : 160 UI, Fer (3b103) : 4mg, Iode (3b202) : 0,28 mg, Cuivre (3b405, 3b406) : 2,2 mg,Manganèse (3b502, 3b503, 3b504) : 1,1 mg, Zinc (3b603,3b605, 3b606) : 11 mg –Clinoptilolited’origine sédimentaire : 2 g. Protéine : 11,0 % - Teneur en matières grasses : 4,5 % - Cendres brutes : 1,7 % - Cellulose brute : 0,5 % - Humidité : 80,0 %.
"""

# Extract and print the nutrition dictionary
nutrition_info = extract_nutrition(text)
print(nutrition_info)


#task 3
def is_grandma(lst):
    for sublist in lst:
        for i in range(len(sublist) - 1):
            if sublist[i] * sublist[i + 1] in sublist:
                return True
    return False
 # Returns: True
print(is_grandma([1, 2, [[4, 5], [4, 7]], 5, 4, [[95], [2]]])) 

