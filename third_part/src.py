#task 1
import json
import gzip

def read_categories(file_path): # takes file as given arugument
    with gzip.open(file_path, 'rt') as f: # open the file in read mode
        data = json.load(f) #load data in json format
    return data

def get_categories(data, category): # takes data and category as argument
    categories = {} # empty dictonary to store the categories
    for subcategory in data.get(category, []): # itreiates over each subcategory inside category
        categories[subcategory] = get_categories(data, subcategory)  #This line recursively calls get_categories for each subcategory and stores the result in the categories dictionary.
    return categories #This line returns the categories dictionary.
    

def build_categories_structure(file_path, root_category): #takes  file_path and  root_category as argument
    data = read_categories(file_path)  #read the data from file
    return get_categories(data, root_category)


print(build_categories_structure('third_part/categories.json.gz', 'root'))

#task 2

import scrapy


class WoolworthscomSpiderSpider(scrapy.Spider):
    name = 'Woolworthscom_spider' #defines the spider name
    allowed_domains = ['woolworths.com.au'] 
    start_urls = ['https://www.woolworths.com.au/shop/browse/drinks/cordials-juices-iced-teas/iced-teas']

    def parse(self, response):
        
        breadcrumb = response.css('.breadcrumbs li a::text').getall() # Extract breadcrumb as given 

        
        product_names = response.css('.shelfProductTile-description::text').getall() # Extract product names

       
        for product_name in product_names:  # Yield each product as an item
            yield {
                'product_name': product_name.strip(),
                'breadcrumb': breadcrumb
            }


